# securefmt
#### [![Build Status]][actions] [![Latest Version]][crates.io] [![Docs]][docs.rs]
[Build Status]: https://gitlab.com/pfouilloux/securefmt/badges/master/pipeline.svg
[actions]: https://gitlab.com/pfouilloux/securefmt/-/pipelines
[Latest Version]: https://img.shields.io/crates/v/securefmt.svg
[crates.io]: https://crates.io/crates/securefmt
[Docs]: https://docs.rs/securefmt/badge.svg
[docs.rs]: https://docs.rs/securefmt

Drop-in replacement for the Debug derive macro that hides fields marked as sensitive.

### Example

The following code snippet
```rust
#[derive(Debug)]
struct SensitiveData {
    id: u8,
    #[sensitive]
    secret: u8
}

fn main() {
    println!("{:?}", SensitiveData { id: 1, secret: 42 })
}
```
will print:
```
SensitiveData { id: 1, secret: <redacted> }
```

If the [debug_mode] feature is active, the same code will print:
```
WARN - WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases.
SensitiveData { id: 1, secret: 42 }
```

#![deny(missing_docs)]
//! Drop-in replacement for the Debug derive macro that hides fields marked as sensitive.
//!
//! # Example
//! The following code snippet
//! ```rust
//! # extern crate securefmt;
//! # use securefmt::Debug;
//!
//! #[derive(Debug)]
//! struct SensitiveData {
//!     id: u8,
//!     #[sensitive]
//!     secret: u8
//! }
//!
//! fn main() {
//!     println!("{:?}", SensitiveData { id: 1, secret: 42 })
//! }
//! ```
//! will print:
//! ```output
//! SensitiveData { id: 1, secret: <redacted> }
//! ```
//!
//! If the `debug_mode` feature is active, the same code will print:
//! ```output
//! WARN - WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases.
//! SensitiveData { id: 1, secret: 42 }
//! ```

// use features::CompileTimeFeatureDiscriminator;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};
// use crate::implementation::SynDebugImplGenerator;

#[macro_use]
mod errors;
mod derive;
mod formatting_data;
mod implementation;

extern crate proc_macro;

/// # Usage
/// ```rust
/// # extern crate securefmt;
/// # use securefmt::Debug;
/// #[derive(Debug)]
/// struct SensitiveData {
///     id: u8,
///     #[sensitive]
///     secret: u8
/// }
/// ```
#[proc_macro_derive(Debug, attributes(sensitive))]
pub fn derive_secure_debug(input: TokenStream) -> TokenStream {
    let derive_input = parse_macro_input!(input as DeriveInput);
    let derive_result = derive::debug(&derive_input.ident, &derive_input.data);

    match derive_result {
        Ok(tokens) => tokens.into(),
        Err(e) => {
            let syn_error = syn::Error::new(e.call_site, e.message);
            let compile_error = syn_error.to_compile_error();
            quote::quote! {
                #compile_error
            }
            .into()
        }
    }
}

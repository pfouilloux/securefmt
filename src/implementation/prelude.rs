use crate::errors::Result;
use proc_macro2::TokenStream;

#[cfg(test)]
use mockall::automock;

#[cfg_attr(test, automock)]
pub trait FmtBodySource {
    fn generate_fmt_body(&self, ident: &str) -> Result<TokenStream>;
}

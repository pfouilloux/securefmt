use crate::errors::Result;

#[cfg(test)]
use mockall::automock;

#[derive(Debug, PartialEq)]
pub enum FormattingData {
    StructData(Vec<FieldFormattingData>),
    EnumData(Vec<VariantFormattingData>),
}

#[derive(Debug, PartialEq)]
pub struct FieldFormattingData {
    pub ident: Option<String>,
    pub sensitive: bool,
}

#[derive(Debug, PartialEq)]
pub struct VariantFormattingData {
    pub ident: String,
    pub fields: Vec<FieldFormattingData>,
}

#[cfg_attr(test, automock)]
pub trait FormattingDataSource {
    fn to_formatting_data(&self) -> Result<FormattingData>;
}

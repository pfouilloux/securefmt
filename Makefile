ROOT_DIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: clean
clean:
	cargo clean

.PHONY: build
build:
	cargo build --color always --verbose

.PHONY: vanilla-tests
vanilla-tests:
	cargo test --color always --all --verbose

.PHONY: debug_mode-tests
debug_mode-tests:
	cargo test --color always --all --verbose --features="debug_mode"

.PHONY: test
test: vanilla-tests debug_mode-tests

.PHONY: lint
lint:
	cargo clippy --all-targets --all-features -- -D warnings -Wclippy::pedantic

.PHONY: fmt-check
fmt-check:
	cargo fmt --all -- --check

.PHONY: fmt
fmt:
	cargo fmt --all

.PHONY: docs
docs:
	cargo doc --all-features --verbose

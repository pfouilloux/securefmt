use log::Level;
use securefmt::Debug;

#[derive(Debug)]
enum TestEnum {
    Unit,
    Unnamed(u8, #[sensitive] u8),
    Named {
        a: u8,
        #[sensitive]
        b: u8,
    },
}

#[cfg(not(feature = "debug_mode"))]
fn main() {
    use assertx::assert_logs_contain_none;

    let logs = assertx::setup_logging_test();

    assert_eq!(format!("{:?}", TestEnum::Unit), "TestEnum::Unit");
    assert_eq!(
        format!("{:?}", TestEnum::Unnamed(1, 2)),
        "TestEnum::Unnamed { 0: 1, 1: <redacted> }"
    );
    assert_eq!(
        format!("{:?}", TestEnum::Named { a: 3, b: 4 }),
        "TestEnum::Named { a: 3, b: <redacted> }"
    );
    assert_logs_contain_none!(
        logs,
        Level::Warn => "WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases."
    );
}

#[cfg(feature = "debug_mode")]
fn main() {
    use assertx::assert_logs_contain_in_order;

    let logs = assertx::setup_logging_test();

    assert_eq!(format!("{:?}", TestEnum::Unit), "TestEnum::Unit");
    assert_eq!(
        format!("{:?}", TestEnum::Unnamed(1, 2)),
        "TestEnum::Unnamed { 0: 1, 1: 2 }"
    );
    assert_eq!(
        format!("{:?}", TestEnum::Named { a: 3, b: 4 }),
        "TestEnum::Named { a: 3, b: 4 }"
    );
    assert_logs_contain_in_order!(
        logs,
        Level::Warn => "WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases."
    );
}

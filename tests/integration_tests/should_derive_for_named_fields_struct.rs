use log::Level;
use securefmt::Debug;

#[derive(Debug)]
struct TestStruct {
    a: u8,
    #[sensitive]
    b: u8,
}

#[cfg(not(feature = "debug_mode"))]
fn main() {
    use assertx::assert_logs_contain_none;

    let logs = assertx::setup_logging_test();

    assert_eq!(
        format!("{:?}", TestStruct { a: 3, b: 4 }),
        "TestStruct { a: 3, b: <redacted> }"
    );
    assert_logs_contain_none!(
        logs,
        Level::Warn => "WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases."
    );
}

#[cfg(feature = "debug_mode")]
fn main() {
    use assertx::assert_logs_contain_in_order;

    let logs = assertx::setup_logging_test();

    assert_eq!(
        format!("{:?}", TestStruct { a: 3, b: 4 }),
        "TestStruct { a: 3, b: 4 }"
    );
    assert_logs_contain_in_order!(
        logs,
        Level::Warn => "WARNING: securefmt debug_mode feature is active. Sensitive data may be leaked. It is strongly recommended to disable debug_mode in production releases."
    );
}
